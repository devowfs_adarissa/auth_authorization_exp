<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
</head>
<body>
        <div class="row">
            <div class="offset-8 col-4 row">
                    @auth
                     <div class="col-3">
                        <a href="{{ url('/home') }}" class="link-secondary">Home</a>
                     </div> 
                    <form action="{{ route('logout') }}" method="post" class="col-3 d-flex">
                        @csrf
                        <button type="submit" class="btn btn-link text-secondary p-0">Logout</button>
                      </form>
                      <div class="col-4">
                        <span class="text-secondary ">{{auth()->user()->nom }}</span>  
                      </div>
                    @endauth
                </div>
            </div>
    <div class="container">
        @yield("contenu")
    </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>    
</body>
</html>