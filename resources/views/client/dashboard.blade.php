@extends("client.layout")
@section("contenu")
<div class="row justify-content-center">
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Bonjour cher client, vous êtes connecté!') }}
                </div>
            </div>
        </div>
    </div>
@endsection