@extends("client.layout")
@section("contenu")
    <div class="row">
        @isset($commande)
            <h2>Détails de la commande numéro {{ $commande->id }}</h2>
                <div class="col-6">
                    <p>Date de création : {{ $commande->date_commande }}</p>
                    <p>Etat : {{ $commande->etat }}</p>
                    <h4>Produits commandés</h4>
                    <table class="table table-striped">
                        @foreach($commande->produits as $produit)
                            <tr>
                                <td><h6>{{ $produit->titre }}</h6></td>
                                @can('modifier_commande', $commande)
                                <form action="{{ route('commande.produit.update', [$commande->id, $produit->id]) }}" method="post">
                                    @csrf
                                    <td><input type="number" value="{{ $produit->pivot->quantite }}" name="quantite"></td>
                                @else
                                    <td>{{ $produit->pivot->quantite }}</td>
                                @endcan
                                <td>{{ $produit->prix}}</td>

                                @can('modifier_commande', $commande)
                                <td>
                                    <button type="submit" class="btn btn-link text-primary "><i class="bi bi-check2-circle"></i> </button>
                                </form>
                                <form action="{{ route('commande.produit.destroy', [$commande->id, $produit->id]) }}" method="post">
                                    @method("DELETE")
                                    @csrf
                                    <button type="submit" class="btn btn-link text-danger "><i class="bi bi-cart-dash-fill" ></i></button>
                                </form>    
                                
                                @endcan
                            </tr>
                        @endforeach
                    </table>

                    <p >Total de la commande : {{ $total }} Dhs</p>

                </div>            
        @endisset
    </div>
@endsection