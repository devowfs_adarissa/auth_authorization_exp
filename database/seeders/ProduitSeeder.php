<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         DB::table("produits")->insert([
            [
                "titre"=>"salle à manger",
                "Description"=>"Table rectangle 180cm-90cm avec 6 chaises",
                "prix"=>5400,
            ],
             [
                "titre"=>"salle à manger",
                "Description"=>"Table ronde 120cm-80cm avec 4 chaises",
                "prix"=>4500,
            ],
             [
                "titre"=>"Lit double",
                "Description"=>"160x200 super qualité",
                "prix"=>4500,
            ],
        ]);
    }
}
