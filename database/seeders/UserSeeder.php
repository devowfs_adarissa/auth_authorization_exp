<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         DB::table("users")->insert([
            [
                "nom"=>"Mohamed Touzani",
                "email"=>"admin@ecommerce.ma",
                "adresse"=>"55, hay saada, fès",
                "role"=>"admin",
                "password"=>Hash::make("admin")
            ],
                [
                "nom"=>"Ahmed Alaoui",
                "email"=>"ahmed@gmail.com",
                "adresse"=>"55, hay nouzha, fès",
                "role"=>"admin",
                "password"=>Hash::make("ahmed")
            ],
              [
                "nom"=>"Noura Doukali",
                "email"=>"noura.d@gmail.com",
                "adresse"=>"4, Bv Mohamed 6, Fès",
                "role"=>"client",
                "password"=>Hash::make("12345678")
            ],
              [
                "nom"=>"Sara Nouri",
                "email"=>"nouri@gmail.com",
                "adresse"=>"8, rue marjane, Meknès",
                "role"=>"client",
                "password"=>Hash::make("azertyui")
            ],
        ]);
    }
}
