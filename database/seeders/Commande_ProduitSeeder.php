<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Commande_ProduitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table("commande_produit")->insert([
            [
                "commande_id"=>"1",
                "produit_id"=>"2",
                "quantite"=>2
            ],
            [
                "commande_id"=>"1",
                "produit_id"=>"3",
                 "quantite"=>1
            ],
             [
                "commande_id"=>"2",
                "produit_id"=>"1",
                 "quantite"=>3
            ],
             [
                "commande_id"=>"2",
                "produit_id"=>"2",
                 "quantite"=>1
            ],
            [
                "commande_id"=>"3",
                "produit_id"=>"1",
                 "quantite"=>2
            ],
             [
                "commande_id"=>"3",
                "produit_id"=>"2",
                 "quantite"=>1
            ],
             [
                "commande_id"=>"3",
                "produit_id"=>"3",
                 "quantite"=>1
            ],
        ]);
    }
}
