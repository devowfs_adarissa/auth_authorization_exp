<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommandeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table("commandes")->insert([
            [
                "date_commande"=>"2022-12-06",
                "user_id"=>3
            ],
            [
                "date_commande"=>"2023-01-22",
                "user_id"=>4
            ],
            [
                "date_commande"=>"2023-03-23",
                "user_id"=>4
            ],

        ]);
    }
}
