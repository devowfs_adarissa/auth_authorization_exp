<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Commande;
use App\Models\User;
use App\Policies\CommandePolicy;
use Illuminate\Auth\Access\Response;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Commande::class=> CommandePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
    //    Gate::define("afficher_commande", function(User $user, Commande $commande){
    //         // return ($user->id==$commande->user_id || $user->role=='admin') ;
    //         return ($user->id==$commande->user_id || $user->role=='admin') ?
    //                     Response::allow():
    //                     Response::deny('Pour accéder à cette page, vous devez être administrateur ou bien propriétaire de cette commande! ');
    //     });

        Gate::define("modifier_commande", function(User $user, Commande $commande){
            return (($user->id==$commande->user_id && $commande->etat!="terminé")) ;
        });
    }
}
