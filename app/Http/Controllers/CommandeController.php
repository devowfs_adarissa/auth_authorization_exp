<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\Produit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;

class CommandeController extends Controller
{

    //  public function __construct()
    // {
    //     $this->authorizeResource(Commande::class, 'commande');
    // }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Commande $commande)
    {
        // $this->authorize('view', $commande);
        //  if (Auth::user()->cannot('view', $commande)) {
        //     abort(403);
        // }
        // Gate::authorize('view', $commande);
        //Gate::authorize('afficher_commande', $commande);
        $total=$commande->produits->sum(function($produit){
            return $produit->prix*$produit->pivot->quantite;
        });
        return view("commandes.show", compact("commande", "total"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Commande $commande)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Commande $commande)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Commande $commande)
    {
        //
    }

    public function modifierProduit(Request $request, Commande $commande, Produit $produit){
        $commande->produits()->updateExistingPivot($produit->id, ['quantite' => $request->quantite]);
        return Redirect::route("commande.show", $commande);
   }
    public function supprimerProduit(Commande $commande, Produit $produit){
        $commande->produits()->detach($produit);
        return Redirect::route("commande.show", $commande);
   }
}
