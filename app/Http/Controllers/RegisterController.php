<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
      public function index(){
        return view ("Auth.register");
    }

    public function register(Request $request){
     
        $request->validate([
            'nom' => 'required|string|max:255',
            'adresse'=>'string|max:300',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'nom' => $request->nom,
            'email' => $request->email,
            'adresse'=>$request->adresse,
            'password' => Hash::make($request->password),
        ]);

        auth()->login($user);

        return redirect()->route('home');
    }
}
