<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
   public function index(){
    return view("Auth.login");
   }

   public function login(Request $request){
         if(Auth::attempt(["email"=>$request->email, "password"=>$request->password])){
            $request->session()->regenerate();
               $role=auth()->user()->role;
                if ($role=='admin') 
                    return redirect()->route('admin.dashboard');
                
                if ($role=='client') 
                    return  redirect()->route('client.dashboard');
            }

            return back()->withErrors([
                    'email' => 'Adresse email invalide!',
                ])->onlyInput('email');
        }


 public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
   }
}