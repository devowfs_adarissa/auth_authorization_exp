<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\client\ClientController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Models\Commande;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware("guest")->group(function(){
    Route::get("/login", [LoginController::class,"index"]);
    Route::post("/login", [LoginController::class,"login"])->name("login");

    Route::get("/register", [RegisterController::class,"index"]);
    Route::post("/register", [RegisterController::class,"register"])->name("register");

});
Route::middleware("auth")->group(function(){
    Route::post("/logout", [LoginController::class,"logout"])->name("logout");

    
});

Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::get("/dashboard", [AdminController::class, "index"])->name("admin.dashboard");    
});
 
Route::middleware(['auth', 'role:client'])->group(function () {
    Route::get("/home", [ClientController::class, "index"])->name("client.dashboard"); 
});

Route::middleware(['auth', 'role:admin,client'])->group(function () {
//    Route::get("/commande/{commande}", [CommandeController::class, "show"])->name("commande.show");
    Route::delete("/commande/{commande}/{produit}", [CommandeController::class, "supprimerProduit"])->name('commande.produit.destroy');
    Route::post("/commande/{commande}/{produit}", [CommandeController::class, "modifierProduit"])->name('commande.produit.update');
});

Route::get('/commande/{commande}', [CommandeController::class, "show"])->can('view','commande');

